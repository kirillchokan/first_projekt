﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exist.ViewModels
{
    public class UserView
    {
        public string Id { get; set; }

        public string UserName { get; set; }
    }
}
