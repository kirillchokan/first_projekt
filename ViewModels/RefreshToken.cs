﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exist.ViewModels
{
    public class RefreshToken
    {
        public string NewToken { get; set; }

        public string AccessToken { get; set; }
    }
}
